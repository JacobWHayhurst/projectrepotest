/**
	HOME.JS GETTING SESSION VILLAIN
*/
window.onload=function(){
	console.log("js is linked");
	getSessionVillain();
}

function getSessionVillain(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status==200){
			let vill =  JSON.parse(xhttp.responseText);
			console.log(vill);
			document.getElementById('villName').innerText=vill.name;
			document.getElementById('villPower').innerText=vill.superpower;
		}
		
	}
	
	xhttp.open("GET", "http://localhost:9011/villains/sessvill");
	xhttp.send();
}