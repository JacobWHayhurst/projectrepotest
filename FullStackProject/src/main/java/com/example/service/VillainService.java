package com.example.service;

import java.util.List;

import com.example.dao.VillainDAOImpl;
import com.example.model.SuperVillain;

public class VillainService {
	
	private VillainDAOImpl vDao;
	
	public VillainService() {
		// TODO Auto-generated constructor stub
	}

	public VillainService(VillainDAOImpl vDao) {
		super();
		this.vDao = vDao;
	}
	
	public List<SuperVillain> findAllVillains(){
		return vDao.getAllVillains();
	}
	
	public SuperVillain findVillainByName(String name) {
		SuperVillain vill = vDao.getVillainByName(name);
		if(vill==null) {
			throw new NullPointerException();
		}
		return vill;
	}
	
	public SuperVillain verifySuperPower(String name, String superpower) {
		
		SuperVillain vill = findVillainByName(name);
		if(vill.getSuperpower().equals(superpower)) {
			return vill;
		}
		return null;
		
	}
	
	

}
