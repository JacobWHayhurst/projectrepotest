package com.example.model;

public class SuperVillain {
	
	private int villainId;
	private String name;
	private String superpower;
	private double bounty;
	
	public SuperVillain() {
		// TODO Auto-generated constructor stub
	}
	
	public SuperVillain(String name, String superpower, double bounty) {
		super();
		this.name = name;
		this.superpower = superpower;
		this.bounty = bounty;
	}

	public SuperVillain(int villainId, String name, String superpower, double bounty) {
		super();
		this.villainId = villainId;
		this.name = name;
		this.superpower = superpower;
		this.bounty = bounty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSuperpower() {
		return superpower;
	}

	public void setSuperpower(String superpower) {
		this.superpower = superpower;
	}

	public double getBounty() {
		return bounty;
	}

	public void setBounty(double bounty) {
		this.bounty = bounty;
	}
	
	public int getVillainId() {
		return villainId;
	}

	@Override
	public String toString() {
		return "SuperVillain [villainId=" + villainId + ", name=" + name + ", superpower=" + superpower + ", bounty="
				+ bounty + "]";
	}

}
