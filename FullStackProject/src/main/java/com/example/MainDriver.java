package com.example;

import com.example.controller.VillainController;
import com.example.dao.VillainDAOImpl;
import com.example.dao.VillainDBConnection;
import com.example.service.VillainService;

import io.javalin.Javalin;

public class MainDriver {

	public static void main(String[] args) {
		
		VillainController vCont = new VillainController(new VillainService(new VillainDAOImpl(new VillainDBConnection())));
		
		Javalin app = Javalin.create(config ->{
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");
		});
		
		app.start(9011);
		
		app.post("/villains/login", vCont.POSTLOGIN);
		
		app.get("/villains/sessvill", vCont.GETSESSVILL);
		
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("/html/badlogin.html");
		});

	}

}
