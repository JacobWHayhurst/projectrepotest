package com.example.dao;

import java.util.List;

import com.example.model.SuperVillain;

public interface VillainDAO {

	public List<SuperVillain> getAllVillains();
	public SuperVillain getVillainByName(String name);
	
}
