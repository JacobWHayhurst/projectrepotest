package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class VillainDBConnection {
	
	private final String url = "jdbc:postgresql://jwa-2112-dec.c7otdgsixoyh.us-east-2.rds.amazonaws.com:5432/villaindb";
	private final String username ="villainuser";
	private final String password ="P4ssw0rd";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}

}
