package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.SuperVillain;

public class VillainDAOImpl implements VillainDAO {
	
	private VillainDBConnection vdc;
	
	public VillainDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	public VillainDAOImpl(VillainDBConnection vdc) {
		super();
		this.vdc = vdc;
	}

	@Override
	public List<SuperVillain> getAllVillains() {
		
		try(Connection con = vdc.getDBConnection()){
			
			String sql = "select * from supervillain";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<SuperVillain> villList =  new ArrayList<>();
			
			while(rs.next()) {
				villList.add(new SuperVillain(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4)));
			}
			return villList;
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public SuperVillain getVillainByName(String name) {
		
		try(Connection con =  vdc.getDBConnection()){
			
			String sql = "select * from supervillain where name=?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			SuperVillain vill = null;
			
			while(rs.next()) {
				vill = new SuperVillain(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4));
			}
			return vill;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
