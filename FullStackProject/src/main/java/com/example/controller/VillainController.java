package com.example.controller;

import com.example.model.SuperVillain;
import com.example.service.VillainService;

import io.javalin.http.Handler;

public class VillainController {
	
	private VillainService vServ;
	
	public VillainController() {
		// TODO Auto-generated constructor stub
	}

	public VillainController(VillainService vServ) {
		super();
		this.vServ = vServ;
	}
	
	public final Handler POSTLOGIN = (ctx) ->{
		
		SuperVillain vill = vServ.verifySuperPower(ctx.formParam("villainname"), ctx.formParam("superpower"));
		if(vill !=null) {
			ctx.sessionAttribute("currentvill", vill); //this method allows us to store a user in a contextual session with key name
			//, for as long as that key is valid, we can access the information stored key value. we can use this 
			//to keep track of a logged in user
			ctx.redirect("/html/home.html");
		}else {
			ctx.redirect("/html/badlogin.html");
		}
	};
	
	public final Handler GETSESSVILL = (ctx) ->{
		System.out.println((SuperVillain)ctx.sessionAttribute("currentvill")); //the session object will be stored as a generic object type, so when grabbing the session
		//attribute, we must cast it back to a SuperVillain Object
		SuperVillain vill = (SuperVillain)ctx.sessionAttribute("currentvill");
		ctx.json(vill);
	};
	

}
